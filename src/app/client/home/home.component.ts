import {Component, OnInit} from '@angular/core';
import {NbLayoutScrollService, NbSpinnerService} from "@nebular/theme";

@Component({
    selector: 'ngx-home',
    templateUrl: './home.component.html',
    styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

    constructor() {
    }

    ngOnInit() {
    }

}
