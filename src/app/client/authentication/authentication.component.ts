import {Component, OnInit} from '@angular/core';
import {NbSpinnerService} from "@nebular/theme";

@Component({
    selector: 'ngx-authentication',
    templateUrl: './authentication.component.html',
    styleUrls: ['./authentication.component.scss']
})
export class AuthenticationComponent implements OnInit {

    constructor() {
    }

    ngOnInit() {
    }

}
