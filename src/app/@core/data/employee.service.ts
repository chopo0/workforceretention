import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {URLService} from "./url.service";
import {Observable} from "rxjs";

const httpOptions = {
    headers: new HttpHeaders({'Content-Type': 'application/json'})
};

@Injectable()
export class EmployeeService {

    constructor(private http: HttpClient, private urlService: URLService) {
    }

    // TODO: observables
    getEmployees(page, perPage, clientId): Observable<any> {

        return this.http.get(this.urlService.baseUrl +
            `/api/v1/clients/employees/${clientId}?page=${page}&perPage=${perPage}`);
    }

    getEmployee(id): Observable<any> {
        return this.http.get(this.urlService.baseUrl + '/api/v1/employees/' + id);
    }

    createEmployee(employee, clientId): Observable<any> {
        const body = JSON.stringify(employee);
        return this.http.post(this.urlService.baseUrl + `/api/v1/employees/${clientId}`, body, httpOptions);
    }

    updateEmployee(role, id): Observable<any> {
        const body = JSON.stringify(role);
        return this.http.put(this.urlService.baseUrl + '/api/v1/employees/' + id, body, httpOptions);
    }

    deleteEmployee(id): Observable<any> {
        return this.http.delete(this.urlService.baseUrl + '/api/v1/employees/' + id);
    }

    uploadEmployees(file, clientId): Observable<any> {
        const formData = new FormData();
        formData.append('employees', file);
        return this.http.post(this.urlService.baseUrl + `/api/v1/employees/upload/${clientId}`, formData);
    }
}
